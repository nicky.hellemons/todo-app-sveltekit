import prisma from '$lib/prisma';
import { json } from '@sveltejs/kit';

export async function PUT({request, params}) {
    const { completed } = await request.json();
    const { id } = params;
    const item = await prisma.todo.update({
        where: {
            id: Number(id),
        },
        data: {
            completed,
        },
    });
    return json(item);
}

export async function DELETE({params}) {
    const { id } = params;
    const item = await prisma.todo.delete({
        where: {
            id: Number(id),
        },
    });
    return json(item);
}
