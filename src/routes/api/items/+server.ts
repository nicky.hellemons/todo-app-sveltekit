import prisma from '$lib/prisma';
import { json } from '@sveltejs/kit';

export async function GET() {
    const items = await prisma.todo.findMany();
    return json(items);
}

export async function POST({request}) {
    const { name } = await request.json();
    const item = await prisma.todo.create({
        data: {
            name,
        },
    });
    return json(item);
}